
#################
# Private subnet
#################
resource "aws_subnet" "Pomelo_private" {
  count = var.create_vpc && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0

  vpc_id                          = aws_vpc.Pomelo_VPC.id
  cidr_block                      = var.private_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  
  tags = {
    Name              = var.private_subnet_name
    creator           = var.subnet_creator
    region            = var.subnet_region
    availability-zone = var.subnet_avail
    environment       = var.subnet_env
    atomation         = var.subnet_atomation
    account           = var.account_id
  }
}

output "subnet_id" {
  value = aws_subnet.Pomelo_private.id
}
  
