################
# Variable private subnet
################


# Input variable of Pomelo private subnet  

variable "create_vpc" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  type        = bool
  default     = true
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = []
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = []
}

variable "private_subnet_name" {
  description = "tagging for the subnet"
  type        = string
  default     = ""
}

variable "subnet_creator"{
 description = "creator user name of resource"
 type        = string
 default    = "Pbarbhui"
}

variable "subnet_region"{
 description = "region for subnet"
 type        = string
 default   = "us-east-1"
}
variable "subnet_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "subnet_atomation"{
 type        = string
 default     = "terraform"
}
variable "subnet_avail"{
 description = "availability zone for subnet"
 type        = string
 default     = "us-east-1a"

}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "007"
}
